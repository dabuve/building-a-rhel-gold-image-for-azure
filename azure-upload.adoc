== Read-Only: How you could upload this machine to Azure

[NOTE]
We cannot actually run these commands in this lab, because 60+ people uploading 
multi-gigabyte images would consume all the bandwidth at {event}, and would 
also take quite some time. This section of the lab is for reading only, we cannot
do these steps on the laptops. 

// include::rhel7azureShell.adoc[]

=== Read-Only: Using the Azure CLI

Enter az login to authenticate your Azure administration server and log in.

	az login

Example:

	root@workstation-GUID: az login
	To sign in, use a web browser to open the page https://aka.ms/devicelogin and enter the code FDMSCMETZ to authenticate.
	  [
		{
		  "cloudName": "AzureCloud",
		  "id": "",
		  "isDefault": true,
		  "name": "",
		  "state": "Enabled",
		  "tenantId": "",
		  "user": {
			"name": "",
			"type": "user"
		  }
		}
	  ]

=== Read-Only: Export a Storage Account Key

Important: The following steps are only for users that have existing resources for the VM in Microsoft Azure. If you need to create new Azure resources, go to Set Up New Resources in Microsoft Azure.

Complete the steps below to get your storage account key and export it to Microsoft Azure.

Get the storage account connection string.

	az storage account show-connection-string -n <storage-account-name> -g <resource-group>


Example:

	root@workstation-GUID: az storage account show-connection-string -n azrhelclistact -g azrhelclirsgrp
	{
	  "connectionString": "DefaultEndpointsProtocol=https;EndpointSuffix=core.windows.net;AccountName=azrhelclistact;AccountKey=NreGk...=="
	}


Export the connection string. Copy the connection string and paste it into the following command. This connects your system to the storage account.

	export AZURE_STORAGE_CONNECTION_STRING="<storage-connection-string>"


Example:

	root@workstation-GUID: export AZURE_STORAGE_CONNECTION_STRING="DefaultEndpointsProtocol=https;EndpointSuffix=core.windows.net;AccountName=azrhelclistact;AccountKey=NreGk...=="

Once you have exported the storage connection string, go to Upload and Provision the Azure RHEL VM.

Set Up New Resources in Microsoft Azure
Complete the following steps to create resources in Microsoft Azure.

Create a resource group in an Azure region.

	az group create --name <resource-group> --location <azure-region>


Example:

	root@workstation-GUID: az group create --name azrhelclirsgrp --location southcentralus
	{
	  "id": "/subscriptions//resourceGroups/azrhelclirsgrp",
	  "location": "southcentralus",
	  "managedBy": null,
	  "name": "azrhelclirsgrp",
	  "properties": {
		"provisioningState": "Succeeded"
	  },
	  "tags": null
	}


Create a storage account. Refer to Storage SKU Types for SKU type descriptions.

	az storage account create -l <azure-region> -n <storage-account-name> -g <resource-group> --sku <sku_type>


Example:

	root@workstation-GUID: az storage account create -l southcentralus -n azrhelclistact -g azrhelclirsgrp --sku Standard_LRS
	{
	  "accessTier": null,
	  "creationTime": "2017-04-05T19:10:29.855470+00:00",
	  "customDomain": null,
	  "encryption": null,
	  "id": "/subscriptions//resourceGroups/azrhelclirsgrp/providers/Microsoft.Storage/storageAccounts/azrhelclistact",
	  "kind": "Storage",
	  "lastGeoFailoverTime": null,
	  "location": "southcentralus",
	  "name": "azrhelclistact",
	  "primaryEndpoints": {
		"blob": "https://azrhelclistact.blob.core.windows.net/",
		"file": "https://azrhelclistact.file.core.windows.net/",
		"queue": "https://azrhelclistact.queue.core.windows.net/",
		"table": "https://azrhelclistact.table.core.windows.net/"
	},
	"primaryLocation": "southcentralus",
	"provisioningState": "Succeeded",
	"resourceGroup": "azrhelclirsgrp",
	"secondaryEndpoints": null,
	"secondaryLocation": null,
	"sku": {
	  "name": "Standard_LRS",
	  "tier": "Standard"
	},
	"statusOfPrimary": "available",
	"statusOfSecondary": null,
	"tags": {},
	  "type": "Microsoft.Storage/storageAccounts"
	}


Get the storage account connection string.

	az storage account show-connection-string -n <storage-account-name> -g <resource-group>


Example:

	root@workstation-GUID: az storage account show-connection-string -n azrhelclistact -g azrhelclirsgrp
	{
	  "connectionString": "DefaultEndpointsProtocol=https;EndpointSuffix=core.windows.net;AccountName=azrhelclistact;AccountKey=NreGk...=="
	}


Export the connection string. Copy the connection string and paste it into the following command. This connects your system to the storage account.

	export AZURE_STORAGE_CONNECTION_STRING="<storage-connection-string>"


Example:

	root@workstation-GUID: export AZURE_STORAGE_CONNECTION_STRING="DefaultEndpointsProtocol=https;EndpointSuffix=core.windows.net;AccountName=azrhelclistact;AccountKey=NreGk...=="


Create the storage container.

	$ az storage container create -n <container-name>


Example:

	root@workstation-GUID: az storage container create -n azrhelclistcont
	{
	  "created": true
	}


Create a virtual network.

	az network vnet create -g <resource group> --name <vnet-name> --subnet-name <subnet-name>


Example:

	root@workstation-GUID: az network vnet create --resource-group azrhelclirsgrp --name azrhelclivnet1 --subnet-name azrhelclisubnet1
	{
	  "newVNet": {
		"addressSpace": {
		  "addressPrefixes": [
		  "10.0.0.0/16"
		  ]
	  },
	  "dhcpOptions": {
		"dnsServers": []
	  },
	  "etag": "W/\"\"",
	  "id": "/subscriptions//resourceGroups/azrhelclirsgrp/providers/Microsoft.Network/virtualNetworks/azrhelclivnet1",
	  "location": "southcentralus",
	  "name": "azrhelclivnet1",
	  "provisioningState": "Succeeded",
	  "resourceGroup": "azrhelclirsgrp",
	  "resourceGuid": "0f25efee-e2a6-4abe-a4e9-817061ee1e79",
	  "subnets": [
		{
		  "addressPrefix": "10.0.0.0/24",
		  "etag": "W/\"\"",
		  "id": "/subscriptions//resourceGroups/azrhelclirsgrp/providers/Microsoft.Network/virtualNetworks/azrhelclivnet1/subnets/azrhelclisubnet1",
		  "ipConfigurations": null,
		  "name": "azrhelclisubnet1",
		  "networkSecurityGroup": null,
		  "provisioningState": "Succeeded",
		  "resourceGroup": "azrhelclirsgrp",
		  "resourceNavigationLinks": null,
		  "routeTable": null
		}
	  ],
	  "tags": {},
	  "type": "Microsoft.Network/virtualNetworks",
	  "virtualNetworkPeerings": null
	  }
	}



=== Read-Only: Upload and Provision the Azure RHEL VM
Complete the following steps to upload and provision the VM. Note that the exported storage connection string does not persist after a system reboot. If any of commands in the following steps fail, export the storage connection string again. (See Steps 3 and 4 in the previous section.)

Upload the image to the storage container. It may take several minutes.

Note: Enter az storage container list to get the list of storage containers.

	az storage blob upload --account-name <storage-account-name> --container-name <container-name> --type page --file <path-to-vhd> --name <image-name>.vhd

Example:

	root@workstation-GUID: az storage blob upload --account-name azrhelclistact --container-name azrhelclistcont --type page --file rhel-image-7.3.vhd --name rhel-image-7.3.vhd
	Percent complete: %100.0

Get the URL for the uploaded vhd file. You will need to use this URL in the following step.

	az storage blob url -c <container-name> -n <image-name>.vhd

Example:

	root@workstation-GUID: az storage blob url -c azrhelclistcont -n rhel-image-7.3.vhd
	"https://azrhelclistact.blob.core.windows.net/azrhelclistcont/rhel-image-7.3.vhd"

=== Read-Only: Create the VM

Note: The following command uses the option --generate-ssh-keys, which creates a private/public key pair. The private and public key files are created in ~/.ssh on your local machine. The public key is added to the authorized_keys file on the VM for the user specified by the --admin-username option.

	az vm create --resource-group <resource-group> --location <azure-region> --use-unmanaged-disk --name <vm-name> --storage-account <storage-account-name> --os-type linux --admin-username <administrator-name> --generate-ssh-keys --image <URL>

Example:

	root@workstation-GUID: az vm create --resource-group azrhelclirsgrp --location southcentralus --use-unmanaged-disk --name rhel-azure-vm-1 --storage-account azrhelclistact --os-type linux --admin-username clouduser --generate-ssh-keys --image https://azrhelclistact.blob.core.windows.net/azrhelclistcont/rhel-image-7.3.vhd

	{
	  "fqdns": "",
	  "id": "/subscriptions//resourceGroups/azrhelclirsgrp/providers/Microsoft.Compute/virtualMachines/rhel-azure-vm-1",
	  "location": "southcentralus",
	  "macAddress": "",
	  "powerState": "VM running",
	  "privateIpAddress": "10.0.0.4",
	  "publicIpAddress": "12.84.121.147",
	  "resourceGroup": "azrhelclirsgrp"

Note the public IP address. You will need this to log in to the VM in the next step.

Start an SSH session and log in to the appliance.

	ssh -i <path-to-ssh-key> <admin-username@public-IP-address>

Example:

	root@workstation-GUID: ssh  -i /home/clouduser/.ssh/id_rsa clouduser@12.84.121.147
	The authenticity of host '12.84.121.147' can't be established.
	Are you sure you want to continue connecting (yes/no)? yes
	Warning: Permanently added '12.84.121.147' (ECDSA) to the list of known hosts.

	[clouduser@rhel-azure-vm-1 ~]$

If you see your user login, you have successfully deployed your Azure RHEL VM.

You can now go to the Microsoft Azure portal and check the audit logs and properties of your resources. You can manage your VMs directly in the Microsoft Azure portal. If you are managing multiple VMs, you should use the Azure CLI. The Azure CLI provides a powerful interface to your resources in Microsoft Azure. Enter az --help in the CLI or go to Azure CLI 2.0 Command Reference to learn more about the commands you use to manage your VMs in Microsoft Azure.

Using other Authentication Methods
While recommended for increased security, the use of the Azure-generated public key file in the example above is not a requirement. The following examples show two other methods for SSH authentication.

Example 1: These command options provision a new Azure VM without generating a public key file. They allow SSH authentication using a password.

	az vm create --resource-group <resource-group> --location <azure-region> --use-unmanaged-disk --name <vm-name> --storage-account <storage-account-name> --os-type linux --admin-username <administrator-name> --admin-password <ssh-password> --image <URL>

Authentication command: ssh <admin-username@public-ip-address>

Example 2: These command options provision a new Azure VM that you can use the SSH protocol to access using an existing public key file.

	az vm create --resource-group <resource-group> --location <azure-region> --use-unmanaged-disk --name <vm-name> --storage-account <storage-account-name> --os-type linux --admin-username <administrator-name> --ssh-dest-key-path <path-to-existing-ssh-key> --image <URL>

Authentication command: ssh -i <path-to-existing-ssh-key> <admin-username@public-ip-address>

