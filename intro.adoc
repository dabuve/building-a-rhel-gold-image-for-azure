== Introduction and Lab Outline

This lab walks you through the process of building a RHEL "gold image" that can be used in a Microsoft Azure cloud environment. 

This process is necessary if your organization wants to use Cloud Access
("Bring your own subscription"), and not Azure Marketplace images. It is important to
understand that Azure Marketplace RHEL images can only be consumed on a Pay As You Go
basis from Azure - you cannot use existing Cloud Access subscriptions.

If you're not familiar with Cloud Access, it's a free feature of most existing Red
Hat subscriptions for RHEL, JBoss, CloudForms, OpenShift and Gluster that allow
 subscriptions to be moved to a public cloud provider. To find out
more about cloud access and how to convert subscriptions, please look at this
link;
https://www.redhat.com/en/technologies/cloud-computing/cloud-access[Overview
to Cloud Access].

[cols="1,3"]
|===
| **Lab Technical Difficulty:** | **Beginner** - Basic command line knowledge is useful, and lots of help is available!
|===

=== Why would I want to learn how to build a gold image for Azure?

- Most companies who use RHEL at scale have their own gold images, with requirements
  for standard service configuration, security settings, and pre-installed packages.
- Most companies already have an existing large number of Red Hat
  Enterprise Linux subscriptions too, and want to bring those to Azure.
- The Azure Marketplace images cannot be customized before they are
  provisioned, and they also cannot be used with existing subscriptions.
  Therefore it is necessary to learn how to build a RHEL gold image for Azure.

=== What will I do in this lab?

. You'll start this lab by logging into a build-workstation running in Red
Hat's cloud. You will have root access on this workstation.
. Setup and install *Image Builder*, the new tool on RHEL8 for building RHEL images
. Create a image "blueprint" with a custom package selection
. Build this image as a VHD (Azure Disk) format.
. Perform standard installation settings changes like hostname, configuration
files, and check that the machine follows Azure best practices - using Azure
specific device drivers and the Azure agent.
. Install the Azure Linux Agent (Often called by the old name - WALA - Windows
Azure Linux Agent).
. Review how the Azure upload tools work.

[NOTE]
This lab produces a virtual machine that is several Gb in size,
which is unpractical to upload to Azure during the {event} due to
bandwidth limits. This lab includes instructions on how you would upload the image using the
Azure tools which you can read through. However, the actual **upload to Azure** is
read only and you will not upload your built images to Azure. You will see how
these steps are quite simple through, so you won't miss much, and you **will
learn lots of Azure best practices for RHEL and all about the new Image Builder in this lab**!

=== What skills do I need for this lab? 

It's recommended that you have some basic Linux skills - Red Hat Certified
Systems Administrator level will be absolutely fine to complete this lab.

Please do ask if you have questions, Red Hatters should always be nearby to
help out!

=== After completing this lab, you should be able...

- To understand how to build a RHEL gold image for Azure
- To understand the reasons behind some of the changes we make for Azure
- Be able to build your own Azure image for use in Cloud Access

=== Author, support and acknowledgments

This lab was authored by **James Read**.

This lab is being supported by **Toni Willberg**. 

Here's what we look like if you want to ask for help!

[cols="^,^"]
|===
| image:labImages/jreadProfile.jpg[jread, 200]             | image:labImages/tonyProfile.jpg[khaled, 200]
| **James Read**                                           | **Toni Willberg**
| EMEA Principal Solution Architect for Microsoft, Red Hat | Cloud  Solution Architect (Open Source), Microsoft
| Email: james@redhat.com                                  | Email: toni.willberg@microsoft.com
|===

James would like to express his sincere thanks to;

* The Lorax Composer developers, including **Brian Lane** for feedback and
  suggestions, as well as some great documentation that was used/copied!
* The **Image Builder** Product Management team for their quick review! 
* To __**you**__ for choosing this lab as part of your {event} experience!

=== Microsoft Feedback ===

Microsoft would like to hear your feedback about your time at Partner Conference!

Feedback form: https://aka.ms/AA5crkq[role="ext-link"]

=== __**Read everything**__!

This lab has been designed for you to learn *how* things work from top to
bottom. This means there are lots of descriptions and reading, not just
commands for you to copy and paste! If you just copy and paste all the commands
you can be done in 30 minutes... but you won't learn anything!

You have plenty of time to complete the lab, take is slow and read everything.
If you get stuck, don't be afraid to ask for help at any time, but the answer
is *probably* in the lab documentation ;-) 

